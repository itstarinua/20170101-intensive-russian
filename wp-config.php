<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'dstar148_samples_works');

/** Имя пользователя MySQL */
define('DB_USER', 'dstar148_samples');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'E$9bS(NVF?oQ');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y7w.?-tN)jsGl,ijZO:GK+y1$jyklKT|O1^q98x39<-{Q3~@iKu0:=?L#N]+/g>S');
define('SECURE_AUTH_KEY',  'P H,3lXZa qS6Z|-PE..P3VIj9+0Cc/q^jy3]YedY(nmV_;#h8-~H*ETg$%Q#-YX');
define('LOGGED_IN_KEY',    'phC2<cDS~|*;r+WG;kQ5|+?0Oyg/x|@^JgPcoGT^x:0wB%ksrLI~!K9n+W+^{`C^');
define('NONCE_KEY',        'c7?==A#Z-w6f-*eI;aQOGdD~,]m?|}m|=66,S5;F2s/bzvDWW<M-aZuA!oP5/bEk');
define('AUTH_SALT',        '7-*rJ9:6rN0I][GOvMDsD&nauuc)5QP=RNo#/-hfG)3j3F}X+>;5s7 l0Ksr@jR2');
define('SECURE_AUTH_SALT', 'Y?sf<LE1T$8wxYB=m%/B70zR5vwAhqAF:.w5v^b-0I:F^C<8wGEDS7E^Gi-C8G|_');
define('LOGGED_IN_SALT',   'd$jx>=--$Uq3N+6{b2-I1%leq2 wR&*O%ECYemHE_j18|b;@6hE~A.g_?V6(q|mP');
define('NONCE_SALT',       '|1 I4t`XM?YUNSM9aZ|&~R1F{op&-GR`-~G>{-^5mU9gKLNkHi-@tm;7q#b8#Zf|');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = '20170101_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');